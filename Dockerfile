FROM arm32v7/python:3.7-bullseye
RUN apt-get update && apt-get install -y libffi-dev gcc libssl-dev
COPY . /app
WORKDIR /app
RUN pip install cryptography==3.3.2 paho-mqtt==1.6.1 broadlink==0.18.3
ENV BROADLINKMQTTCONFIG="/app/mqtt.conf"
ENV BROADLINKMQTTCONFIGCUSTOM="/app/custom.conf"
CMD ["python", "mqtt.py"]
